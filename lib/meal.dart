import 'package:daily_cal/addMeal.dart';
import 'package:daily_cal/editMeal.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MealWidget extends StatefulWidget {
  MealWidget({Key? key}) : super(key: key);

  @override
  _MealWidgetState createState() => _MealWidgetState();
}

class _MealWidgetState extends State<MealWidget> {
  List<String> meals = [];
  List<String> mealName = [];
  List<String> mealCal = [];
  var _tapPosition;
  int selectedIndex = -1;

  @override
  void initState() {
    super.initState();
    _loadMeals();
    _tapPosition = Offset(0.0, 0.0);
  }

  _loadMeals() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      meals = prefs.getStringList('meals') ?? [];
      mealName = prefs.getStringList('mealName') ?? [];
      mealCal = prefs.getStringList('mealCal') ?? [];
    });
  }

  _saveMeals() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('mealName', mealName);
      prefs.setStringList('mealCal', mealCal);
    });
  }

  _saveIndex() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setInt('selectedIndex', selectedIndex);
    });
  }

  _onPop() {
    _loadMeals();
    setState(() {});
    _saveMeals();
  }

  showPopupMenu(int foodIndex) async {
    selectedIndex = foodIndex;
    _saveMeals();
    _saveIndex();
    await showMenu(
      context: context,
      position: RelativeRect.fromRect(
        _tapPosition & Size(40, 40),
        Offset.zero & Size(60, 60),
      ),
      items: [
        PopupMenuItem(
          value: 'edit',
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(Icons.edit),
                Container(padding: EdgeInsets.only(left: 8)),
                Text("edit")
              ]),
        ),
        PopupMenuItem(
            value: 'del',
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(Icons.delete_forever_rounded),
                  Container(padding: EdgeInsets.only(left: 8)),
                  Text("delete"),
                ])),
      ],
      elevation: 8.0,
    ).then((value) {
      if (value == 'edit'){
        Navigator.push(context,
                MaterialPageRoute(builder: (context) => EditMealWidget()))
            .then((value) => _onPop());
      }else if (value == 'del') {
        mealName.removeAt(selectedIndex);
        mealCal.removeAt(selectedIndex);
        _saveMeals();
      }
    });
  }

  void _storePosition(TapDownDetails details) {
    _tapPosition = details.globalPosition;
  }


  Widget _buidMealBox(String foodName, String foodCal, int foodIndex) {
    return GestureDetector(
        onTapDown: _storePosition,
        onLongPress: () {
          showPopupMenu(foodIndex);
        },
        child: Container(
          margin: EdgeInsets.all(6.0),
          decoration: BoxDecoration(
            color: Colors.orange.shade200,
            // border: Border.all(
            //   color: Colors.black,
            //   width: 2,
            // ),
            borderRadius: BorderRadius.circular(8.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black45.withOpacity(0.5),
                blurRadius: 1,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Container(
            margin: EdgeInsets.all(24.0),
            child: Row(
              children: [Text(foodName), Text(foodCal + "  kCal")],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Meal')),
      body: Container(
        margin: EdgeInsets.all(16),
        child: ListView(
          children: [
            Text('$mealName'),
            Text('$mealCal'),
            for (var i = 0; i < mealName.length; i++)
              _buidMealBox(mealName[i], mealCal[i], i),
            Container(
              margin: EdgeInsets.all(8),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(10),
                  shape: CircleBorder(),
                  elevation: 10.0,
                  shadowColor: Colors.black,
                ),
                onPressed: () async {
                  _saveMeals();
                  await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddMealWidget()))
                      .then((value) => _onPop());
                },
                child: Icon(Icons.add_circle_outline,
                    color: Colors.white, size: 40),
              ),
            ),
            Container(
              margin: EdgeInsets.all(8),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(10),
                  shape: CircleBorder(),
                  elevation: 10.0,
                  shadowColor: Colors.black,
                ),
                onPressed: () async {
                  _onPop();
                },
                child: Icon(Icons.refresh_sharp, color: Colors.white, size: 40),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
