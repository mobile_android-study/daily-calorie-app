import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({Key? key}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final _formKey = GlobalKey<FormState>();
  String gender = 'M'; // M or F
  int age = 0;
  int height = 0; // in cm
  int weight = 0; // in kg
  double activity = 1.20;
  double cals = 0.00;

  final _ageController = TextEditingController();
  final _heightController = TextEditingController();
  final _weightController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
      _ageController.text = '$age';
      height = prefs.getInt('height') ?? 0;
      _heightController.text = '$height';
      weight = prefs.getInt('weight') ?? 0;
      _weightController.text = '$weight';
      activity = prefs.getDouble('activity') ?? 1.2;
      cals = prefs.getDouble('cals') ?? 0;
    });
  }

  Future<void> _saveProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('gender', gender);
      prefs.setInt('age', age);
      prefs.setInt('height', height);
      prefs.setInt('weight', weight);
      prefs.setDouble('activity', activity);
      prefs.setDouble('cals', cals);
    });
  }

  void _calculateCals() {
    if (gender == 'M') {
      cals = (66 + (13.7 * weight) + (5 * height) - (6.8 * age)) * activity;
    } else {
      cals = (665 + (9.6 * weight) + (1.8 * height) - (4.7 * age)) * activity;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.all(16.0),
          children: [
            // Age field
            TextFormField(
              controller: _ageController,
              validator: (value) {
                var num = int.tryParse(value!);
                if (num == null || num <= 0) {
                  return 'Please enter your age that more than 0 ';
                }
                return null;
              },
              decoration: InputDecoration(labelText: "age"),
              onChanged: (value) {
                setState(() {
                  age = int.tryParse(value)!;
                });
              },
              keyboardType: TextInputType.number,
            ),

            //Gender
            DropdownButtonFormField(
              value: gender,
              items: [
                //male
                DropdownMenuItem(
                    child: Row(children: [
                      Icon(Icons.male),
                      SizedBox(width: 8.0),
                      Text('male'),
                    ]),
                    value: 'M'),
                //female
                DropdownMenuItem(
                    child: Row(children: [
                      Icon(Icons.female),
                      SizedBox(width: 8.0),
                      Text('female'),
                    ]),
                    value: 'F'),
              ],
              onChanged: (String? newValue) {
                setState(() {
                  gender = newValue!;
                });
              },
            ),

            //Height field
            TextFormField(
              controller: _heightController,
              validator: (value) {
                var num = int.tryParse(value!);
                if (num == null || num <= 0) {
                  return 'Please enter number that more than 0 ';
                }
                return null;
              },
              decoration: InputDecoration(labelText: "height"),
              onChanged: (value) {
                setState(() {
                  height = int.tryParse(value)!;
                });
              },
              keyboardType: TextInputType.number,
            ),

            //Weight field
            TextFormField(
              controller: _weightController,
              validator: (value) {
                var num = int.tryParse(value!);
                if (num == null || num <= 0) {
                  return 'Please enter number that more than 0 ';
                }
                return null;
              },
              decoration: InputDecoration(labelText: "weight"),
              onChanged: (value) {
                setState(() {
                  weight = int.tryParse(value)!;
                });
              },
              keyboardType: TextInputType.number,
            ),

            //Activity
            DropdownButtonFormField(
              value: activity,
              items: [
                //male
                DropdownMenuItem(
                    child: Row(children: [
                      Text('No regular exercise'),
                    ]),
                    value: 1.20),
                //female
                DropdownMenuItem(
                    child: Row(children: [
                      Text('Exercise 1-3 days per week'),
                    ]),
                    value: 1.375),
                DropdownMenuItem(
                    child: Row(children: [
                      Text('Exercise 3-5 days per week'),
                    ]),
                    value: 1.55),
                DropdownMenuItem(
                    child: Row(children: [
                      Text('Exercise almost everyday'),
                    ]),
                    value: 1.725),
                DropdownMenuItem(
                    child: Row(children: [
                      Text('Heavy exercise / athlete'),
                    ]),
                    value: 1.9),
              ],
              onChanged: (double? newValue) {
                setState(() {
                  activity = newValue!;
                });
              },
            ),

            Container(margin: EdgeInsets.only(top: 20)),

            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)
                  ),
                  elevation: 10.0,
                  shadowColor: Colors.black,
                ),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _calculateCals();
                  _saveProfile();
                  Navigator.pop(context);
                }
              },
              child: Container(
                padding: EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.save),
                    Container(padding: EdgeInsets.only(right: 6)), 
                    Text('Save')],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
