import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddMealWidget extends StatefulWidget {
  AddMealWidget({Key? key}) : super(key: key);

  @override
  _AddMealWidgetState createState() => _AddMealWidgetState();
}

class _AddMealWidgetState extends State<AddMealWidget> {
  final _formKey = GlobalKey<FormState>();
  List<String> mealName = [''];
  List<String> mealCal = [''];
  String newMealName = '';
  String newMealCal = '';

  final _nameController = TextEditingController();
  final _mealCalController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      mealName = prefs.getStringList('mealName') ?? [''];
      mealCal = prefs.getStringList('mealCal') ?? [];
    });
  }

  Future<void> _saveMeal() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('mealName', mealName);
      prefs.setStringList('mealCal', mealCal);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Meal"),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.all(16.0),
          children: [
            // Text('$mealName'),
            // Text('$mealCal'),

            // Name TextField
            TextFormField(
                controller: _nameController,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 3) {
                    return 'Please enter more than 3 characters';
                  }
                  return null;
                },
                decoration: InputDecoration(labelText: "meal name"),
                onChanged: (value) {
                  setState(() {
                    newMealName = value;
                  });
                }),

            // calories field
            TextFormField(
              controller: _mealCalController,
              validator: (value) {
                var num = int.tryParse(value!);
                if (num == null || num <= 0) {
                  return 'Please enter a value more than 0 ';
                }
                return null;
              },
              decoration: InputDecoration(labelText: "calories"),
              onChanged: (value) {
                setState(() {
                  newMealCal = '$value';
                });
              },
              keyboardType: TextInputType.number,
            ),

            Container(margin: EdgeInsets.only(top: 20)),

            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  mealName.add(newMealName);
                  mealCal.add(newMealCal);
                  _saveMeal();
                  Navigator.pop(context);
                }
              },
              child: Container(
                padding: EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.save),
                    Container(padding: EdgeInsets.only(right: 6)),
                    Text('Save')
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
