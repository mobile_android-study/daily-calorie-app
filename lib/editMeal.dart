import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditMealWidget extends StatefulWidget {
  EditMealWidget({Key? key}) : super(key: key);

  @override
  _EditMealWidgetState createState() => _EditMealWidgetState();
}

class _EditMealWidgetState extends State<EditMealWidget> {
  final _formKey = GlobalKey<FormState>();
  List<String> mealName = [''];
  List<String> mealCal = [''];
  String newMealName = '';
  String newMealCal = '';
  int editIndex = 0;

  final _nameController = TextEditingController();
  final _mealCalController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadEditData();
  }

  Future<void> _loadEditData() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      mealName = prefs.getStringList('mealName') ?? [''];
      mealCal = prefs.getStringList('mealCal') ?? [];
      editIndex = prefs.getInt('selectedIndex') ?? 0;
      _nameController.text = mealName[editIndex];
      _mealCalController.text = mealCal[editIndex];
      newMealName = mealName[editIndex];
      newMealCal = mealCal[editIndex];
    });
  }

  Future<void> _saveMeal() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      mealName[editIndex] = newMealName;
      mealCal[editIndex] = newMealCal;
      prefs.setStringList('mealName', mealName);
      prefs.setStringList('mealCal', mealCal);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Meal"),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.all(16.0),
          children: [
            // Text('$mealName'),
            // Text('$mealCal'),

            // Name TextField
            TextFormField(
                controller: _nameController,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 3) {
                    return 'Please enter more than 3 characters';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: "meal name"),
                onChanged: (value) {
                  setState(() {
                    newMealName = value;
                  });
                }),

            // calories field
            TextFormField(
              controller: _mealCalController,
              validator: (value) {
                var num = int.tryParse(value!);
                if (num == null || num <= 0) {
                  return 'Please enter a value more than 0';
                }
                return null;
              },
              autovalidateMode: AutovalidateMode.onUserInteraction,
              decoration: InputDecoration(labelText: "calories"),
              onChanged: (value) {
                setState(() {
                  newMealCal = '$value';
                });
              },
              keyboardType: TextInputType.number,
            ),

            Container(margin: EdgeInsets.only(top: 20)),

            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _saveMeal();
                  Navigator.pop(context);
                }
              },
              child: Container(
                padding: EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.save),
                    Container(padding: EdgeInsets.only(right: 6)),
                    Text('Save')
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
