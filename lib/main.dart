import 'package:daily_cal/meal.dart';
import 'package:daily_cal/profile.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    title: "Daily Calories",
    theme: ThemeData(primarySwatch: Colors.orange, fontFamily: 'Gravity'),
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String name = '';
  String gender = 'M';
  int age = 0;
  var cals = 0.0;
  List<String> mealName = [''];
  List<String> mealCal = ['0'];
  int sumCals = 0;
  var remainCals = 0.0;
  Map<String, double> dataMap = {"loading" : 0};

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  Future<void> _loadData() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      cals = prefs.getDouble('cals') ?? 0;
      mealName = prefs.getStringList('mealName') ?? ['0'];
      mealCal = prefs.getStringList('mealCal') ?? ['0'];
      sumCals = 0;
      remainCals = cals;
      for (int i = 0; i < mealCal.length; i++) {
        sumCals += int.parse(mealCal[i]);
      }
      remainCals = cals - sumCals;

      Map<String, double> mapStr = {};
      for (int i = 0; i < mealCal.length; i++) {
        mapStr[mealName[i]] = double.parse(mealCal[i]);
      }
      dataMap = mapStr;
    });
  }

  Widget _createDrawerItem(IconData icon, String text, Widget routeWidget) {
    return ListTile(
      title: Container(
          margin: const EdgeInsets.all(10.0),
          child: Row(
            children: [
              Icon(icon),
              Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: Text(text,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                    )),
              )
            ],
          )),
      onTap: () async {
        await Navigator.push(
            context, MaterialPageRoute(builder: (context) => routeWidget));
        await _loadData();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Overview')),
        drawer: Drawer(
          child: ListView(
            children: [
              GestureDetector(
                child: DrawerHeader(
                  child: Stack(children: <Widget>[
                    Positioned(
                        bottom: 12.0,
                        left: 8.0,
                        child: Text('Daily Calories',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Qualy',
                                fontSize: 28.0,
                                fontWeight: FontWeight.w600))),
                  ]),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('../assets/image/cover.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              _createDrawerItem(
                  Icons.account_circle, 'Profile', ProfileWidget()),
              _createDrawerItem(Icons.dining_rounded, 'Meal', MealWidget()),
            ],
          ),
        ),
        body: ListView(
          children: [
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(16.0),
                  ),
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text(
                      "Daily Calories : ${cals.toStringAsFixed(0)} kCal",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 30.0),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      "Remain Calories : ${remainCals.toStringAsFixed(0)} kCal",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 30.0,
                          color: (remainCals <= 0)
                              ? Colors.red.withOpacity(0.6)
                              : Colors.green.withOpacity(0.6)),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(24.0),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(48),
              child: PieChart(
                dataMap: dataMap,
                colorList: [
                  Colors.red.shade300,
                  Colors.deepOrangeAccent.shade100,
                  Colors.orange.shade200,
                  Colors.amber.shade200,
                  Colors.yellow.shade200,
                  Colors.yellow.shade100,
                ],
                chartValuesOptions: ChartValuesOptions(
                  showChartValueBackground: false,
                  showChartValues: true,
                  showChartValuesInPercentage: true,
                  showChartValuesOutside: false,
                  decimalPlaces: 1,
                ),
              ),
            )
          ],
        ));
  }
}
